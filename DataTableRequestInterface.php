<?php

namespace Officient\DataTables;

/**
 * Interface DataTableRequestInterface
 * @package Officient\DataTables
 */
interface DataTableRequestInterface
{
    /**
     * @return int
     */
    public function getStart(): int;

    /**
     * @return int|null
     */
    public function getLength(): ?int;

    /**
     * @return array
     */
    public function getOrderBy(): array;

    /**
     * @return array
     */
    public function getColumns(): array;
}