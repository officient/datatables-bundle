<?php

namespace Officient\DataTables;

use Officient\OrmEssentials\Criterion;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DataTableRequest
 * @package Officient\DataTables
 */
class DataTableRequest implements DataTableRequestInterface
{
    /**
     * @var int
     */
    private $start;

    /**
     * @var int
     */
    private $length;

    /**
     * @var array
     */
    private $orderBy;

    /**
     * @var array
     */
    private $columns;

    /**
     * @var array
     */
    private $criteria;

    /**
     * DataTableRequest constructor.
     * @param Request $request
     * @throws \Exception
     */
    public function __construct(Request $request)
    {
        $this->start = $request->query->getInt('start', 0);
        $this->length = $request->query->get('length', null);
        $this->orderBy = [];

        $columns = $request->query->get('columns', []);
        $orders = $request->query->get('order', []);
        foreach($orders as $order) {
            $columnIndex = $order['column'];
            $direction = $order['dir'];
            $column = $columns[$columnIndex];
            $this->orderBy[$column['name']] = strtoupper($direction);
        }

        $this->columns = [];
        foreach($columns as $column) {
            $this->columns[$column['name']] = $column;
        }

        $this->criteria = Search::processColumns($columns);
    }

    /**
     * @inheritDoc
     */
    public function getStart(): int
    {
        return $this->start;
    }

    /**
     * @inheritDoc
     */
    public function getLength(): ?int
    {
        return $this->length;
    }

    /**
     * @inheritDoc
     */
    public function getOrderBy(): array
    {
        return $this->orderBy;
    }

    /**
     * @inheritDoc
     */
    public function getColumns(): array
    {
        return $this->columns;
    }

    /**
     * @param array $merge
     * @return Criterion[]
     */
    public function getCriteria(array $merge = []): array
    {
        return array_merge($this->criteria, $merge);
    }
}