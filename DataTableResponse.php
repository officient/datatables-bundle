<?php

namespace Officient\DataTables;

use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class DataTableResponse
 * @package Officient\DataTables
 */
class DataTableResponse extends JsonResponse implements DataTableResponseInterface
{
    /**
     * @inheritDoc
     */
    public function __construct(array $data, int $recordsFiltered, int $recordsTotal, int $status = 200, array $headers = [])
    {
        parent::__construct([
            'data' => $data,
            'recordsFiltered' => $recordsFiltered,
            'recordsTotal' => $recordsTotal
        ], $status, $headers);
    }
}