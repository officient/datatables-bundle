<?php

namespace Officient\DataTables;

use Officient\OrmEssentials\Criterion;
use Carbon\Carbon;
use Carbon\Exceptions\InvalidFormatException;

class Search
{
    /**
     * Search private constructor.
     */
    private function __construct()
    {
    }

    /**
     * @param array $columns
     * @return Criterion[]
     * @throws \Exception
     */
    public static function processColumns(array $columns): array
    {
        $criteria = array();

        foreach($columns as $column) {
            $criterion = self::processColumn($column);
            if(is_null($criterion) === false) {
                $criteria[] = $criterion;
            }
        }

        return $criteria;
    }

    /**
     * @param array $column
     * @return Criterion|null
     * @throws \Exception
     */
    private static function processColumn(array $column): ?Criterion
    {
        $field = $column['data'];
        $value = trim($column['search']['value']);
        if($column['searchable'] && $value !== "") {
            if(self::isBlankSearch($value)) {
                return self::getBlankSearch($field, $value);
            } else if(self::isDateTimeRange($value)) {
                return self::getDateTimeRangeSearch($field, $value);
            } else if(self::isDateRange($value)) {
                return self::getDateRangeSearch($field, $value);
            } else if(self::isNumberRange($value)) {
                return self::getNumberRangeSearch($field, $value);
            } else {
                return self::getTextSearch($field, $value);
            }
        }

        return null;
    }

    /**
     * @param string $field
     * @param string $value
     * @return Criterion
     * @throws \Exception
     */
    private static function getTextSearch(string $field, string $value): Criterion
    {
        return new Criterion($field, "LIKE", "%{$value}%");
    }

    /**
     * @param string $field
     * @param string $value
     * @return Criterion
     * @throws \Exception
     */
    private static function getDateTimeRangeSearch(string $field, string $value): Criterion
    {
        $valueArr = explode('to', $value);
        foreach($valueArr as &$v) {
            $v = trim($v);
        }

        $from = Carbon::createFromFormat('Y-m-d H:i:s', $valueArr[0])->format("Y-m-d H:i:s");
        $to = Carbon::createFromFormat('Y-m-d H:i:s', $valueArr[1])->format("Y-m-d H:i:s");

        return new Criterion($field, "BETWEEN", $from, $to);
    }

    /**
     * @param string $field
     * @param string $value
     * @return Criterion
     * @throws \Exception
     */
    private static function getDateRangeSearch(string $field, string $value): Criterion
    {
        $valueArr = explode('to', $value);
        foreach($valueArr as &$v) {
            $v = trim($v);
        }

        $from = Carbon::createFromFormat('Y-m-d', $valueArr[0])->format("Y-m-d");
        $to = Carbon::createFromFormat('Y-m-d', $valueArr[1])->format("Y-m-d");

        return new Criterion($field, "BETWEEN", $from, $to);
    }

    /**
     * @param string $field
     * @param string $value
     * @return Criterion
     * @throws \Exception
     */
    private static function getNumberRangeSearch(string $field, string $value): Criterion
    {
        $valueArr = explode('to_num', $value);
        foreach($valueArr as &$v) {
            $v = trim($v);
        }

        $from = $valueArr[0];
        $to = $valueArr[1];

        return new Criterion($field, "BETWEEN", $from, $to);
    }

    /**
     * @param string $field
     * @param string $value
     * @return Criterion
     * @throws \Exception
     */
    private static function getBlankSearch(string $field, string $value): Criterion
    {
        return new Criterion($field, 'IS EMPTY');
    }

    /**
     * @param string $value
     * @return bool
     */
    private static function isDateTime(string $value): bool
    {
        try {
            Carbon::createFromFormat('Y-m-d H:i:s', $value);
            return true;
        } catch (InvalidFormatException $e) {
            return false;
        }
    }

    /**
     * @param string $value
     * @return bool
     */
    private static function isDate(string $value): bool
    {
        try {
            Carbon::createFromFormat('Y-m-d', $value);
            return true;
        } catch (InvalidFormatException $e) {
            return false;
        }
    }

    /**
     * @param string $value
     * @deprecated
     * @return bool
     */
    private static function isRange(string $value): bool
    {
        if(self::isNumberRange($value)) {
            return true;
        }

        if(self::isDateTimeRange($value)) {
            return true;
        }

        if(self::isDateRange($value)) {
            return true;
        }

        return false;
    }

    /**
     * @param string $value
     * @return bool
     */
    private static function isNumberRange(string $value): bool
    {
        $re = '/\d* to_num \d*/';
        return preg_match($re, $value, $matches, PREG_OFFSET_CAPTURE, 0) != false;
    }

    /**
     * @param string $value
     * @return bool
     */
    private static function isDateTimeRange(string $value): bool
    {
        $re = '/\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2} to \d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}/';
        return preg_match($re, $value, $matches, PREG_OFFSET_CAPTURE, 0) != false;
    }

    /**
     * @param string $value
     * @return bool
     */
    private static function isDateRange(string $value): bool
    {
        $re = '/\d{4}-\d{2}-\d{2} to \d{4}-\d{2}-\d{2}/';
        return preg_match($re, $value, $matches, PREG_OFFSET_CAPTURE, 0) != false;
    }

    /**
     * @param string $value
     * @return bool
     */
    private static function isBlankSearch(string $value): bool
    {
        return $value === "***BLANK***";
    }
}