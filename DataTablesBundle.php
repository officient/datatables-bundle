<?php

namespace Officient\DataTables;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class DataTablesBundle
 *
 * This class just needs to be here, because of stuff
 * in symfony documentation...
 *
 * @package Officient\DataTables
 */
class DataTablesBundle extends Bundle
{

}